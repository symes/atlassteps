// Created by Scott Symes

import Combine
import Foundation

@MainActor
class TodayViewModel: ObservableObject {

	@Service private var pedometerService: PedometerProtocol
	@Service private var localStorageService: LocalStorageProtocol
	@Service private var apiService: APIProtocol
	
	@Published var stepCounter: PlottableStepCount = .init(0) // Updated every step. used for top bar chart
	@Published var dailyStepCount: PlottableStepCount = .init(0) // updated every 5 min, used for hourly chart
	@Published var stepData: [DateSteps] = .init(repeating: .init(date: .now, steps: .init(0)), count: 24)
	@Published var error: Error? {
		didSet {
			if let error { print(error) } // TODO: Handle Errors in UI
		}
	}

	private var updateDelay: TimeInterval = 300
	private var timer: Timer?
	private var lastStepRecord: StepsRecord?
	private var currentStepRecord: StepsRecord = .init(
		id: 0,
		username: "scott", // should really come from a user service
		date: .now,
		stepsInPeriod: 0,
		stepsInDay: 0
	)
	private var calendar: Calendar
	private var cancellables = [AnyCancellable]()
	
	private var isNewDay: Bool {
		guard let lastStepRecord else { return true }
		let lastDay = Calendar.current.component(.day, from: lastStepRecord.date)
		let currentDay = Calendar.current.component(.day, from: .now)
		return lastDay != currentDay
	}

	init(calendar: Calendar = .current) {
		self.calendar = calendar
		restoreData()
		setupBindings()
	}

}

private extension TodayViewModel {

	func restoreData() {
		do {
			#if targetEnvironment(simulator)
			let records = generateMockLocalData()
			#else
			let records = try localStorageService.getRecords()
			#endif
			self.stepData = recordsIntoHourBuckets(records)
			self.lastStepRecord = records.last
			self.dailyStepCount = .init(stepData.map(\.steps.stepCount).reduce(0, +))
			self.stepCounter = .init(dailyStepCount.stepCount)
			self.currentStepRecord = .init(
				id: (records.last?.id ?? 0) + 1,
				username: "scott",
				date: .now,
				stepsInPeriod: 0,
				stepsInDay: dailyStepCount.stepCount
			)
		} catch {
			self.error = error
		}
	}

	func setupBindings() {
		pedometerService.stepCountPublisher
			.sink { [weak self] steps in
				guard let self else { return }
				self.stepCounter = .init(self.stepCounter.stepCount + steps)
			}
			.store(in: &cancellables)
		
		do {
			try self.pedometerService.startStepUpdates()
		} catch {
			self.error = error
		}

		timer = Timer.scheduledTimer(
			timeInterval: updateDelay,
			target: self,
			selector: #selector(timerFired),
			userInfo: nil,
			repeats: true
		)
		timer?.fire()
	}

	@objc func timerFired() {
		Task { [weak self] in
			guard let self else { return }
			do {
				// Retreive pedometer data
				let stepsIn5Min = try await self.pedometerService.stepCountFrom(minutes: Int(updateDelay) / 60, before: .now)

				// update current record
				currentStepRecord.stepsInPeriod = stepsIn5Min
				currentStepRecord.date = .now
				currentStepRecord.stepsInDay = currentStepRecord.stepsInDay + stepsIn5Min

				try updateData(with: stepsIn5Min)

				// Post latest record to API
				let responseRecord: StepsRecord = try await self.apiService.call(.postSteps(currentStepRecord))
				_ = responseRecord.username == "scott"

				// Update lastRecord & prepare current record for next data
				lastStepRecord = currentStepRecord
				currentStepRecord.id = currentStepRecord.id + 1
			} catch {
				self.error = error
			}
		}
	}

	func updateData(with steps: Int) throws {
		if isNewDay {
			dailyStepCount = .init(steps)
			stepData = recordsIntoHourBuckets([currentStepRecord])
			try localStorageService.replaceAll(with: [currentStepRecord])
		} else {
			dailyStepCount = .init(dailyStepCount.stepCount + steps)
			stepData.append(currentStepRecord)
			try localStorageService.add(record: currentStepRecord)
		}
	}

	func recordsForDay(_ records: [StepsRecord]) -> [StepsRecord] {
		records
			.filter { $0.date <= .now }
			.filter { $0.date >= Calendar.current.startOfDay(for: .now) }
	}

	func recordsIntoHourBuckets(_ records: [StepsRecord]) -> [DateSteps] {
		var result: [DateSteps] = {
			(0..<24)
				.map {
					.init(
						date: calendar.date(byAdding: .init(hour: $0), to: calendar.startOfDay(for: .now))!,
						steps: .init(0)
					)
				}
		}()

		for record in recordsForDay(records) {
			let hour = calendar.component(.hour, from: record.date)
			if let index = result.firstIndex(where: { calendar.component(.hour, from: $0.date) == hour }) {
				result[index].steps = .init(result[index].steps.stepCount + record.stepsInPeriod)
			}
		}
		return result
	}

	#if targetEnvironment(simulator)
	func generateMockLocalData() -> [StepsRecord] {
		let startOfDay = Calendar.current.startOfDay(for: .now)
		return stride(from: 0, to: Date().timeIntervalSince(startOfDay), by: 60).enumerated()
			.map {
				StepsRecord(
					id: $0.0,
					username: "scott",
					date: startOfDay.addingTimeInterval(Double($0.1)),
					stepsInPeriod: Int.random(in: 0..<15),
					stepsInDay: 0
				)
			}
	}
	#endif

}
