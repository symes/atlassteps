// Created by Scott Symes
#if targetEnvironment(simulator)
import Combine
import Foundation

class MockPedometerService: PedometerProtocol {

	private var step = PassthroughSubject<Int, Never>()
	var stepCountPublisher: AnyPublisher<Int, Never> {
		step.eraseToAnyPublisher()
	}

	lazy var mockData: [(Date, Int)] = generateMockData()
	private var timer: Timer?

	init() {
		mockData = generateMockData()
		do {
			try startStepUpdates()
		} catch {
			print(error) // OK for mock service
		}
	}

	func startStepUpdates() throws {
		timer = Timer.scheduledTimer(
			timeInterval: 1,
			target: self,
			selector: #selector(fireTimer),
			userInfo: nil,
			repeats: true
		)
	}

	@objc func fireTimer() {
		step.send(Int.random(in: 0...30))
	}

	func stepCountFrom(minutes: Int, before end: Date) async throws -> Int {
		stepCount(from: end.addingTimeInterval(Double(-60 * minutes)), to: end)
	}

	func stepCountSinceStartOfDay(until end: Date) async throws -> Int {
		stepCount(from: Calendar.current.startOfDay(for: end), to: end)
	}

}

private extension MockPedometerService {

	func stepCount(from start: Date, to end: Date) -> Int {
		mockData
			.filter { $0.0 <= end }
			.filter { $0.0 >= start }
			.map(\.1)
			.reduce(0, +)
	}

	func generateMockData() -> [(Date, Int)] {
		stride(from: -2000, to: -20, by: 1)
		.map {
			(Date().addingTimeInterval(Double($0 * 10)), Int.random(in: 0..<15))
		}
	}

}
#endif
