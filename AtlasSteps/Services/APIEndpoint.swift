// Created by Scott Symes

import Foundation

enum APIEndpoint {
	case auth(UserAuth)
	case getSteps
	case postSteps(StepsRecord) // N.B. This endpoint doesn't update records received in subsequent `get` operations. Data seems to be ignored

	var path: String {
		switch self {
		case .auth:
			return "auth/local"
		case .getSteps, .postSteps:
			return "steps"
		}
	}

	var method: HTTPMethod {
		switch self {
		case .getSteps:
			return .get
		case .auth, .postSteps:
			return .post
		}
	}

	var data: Encodable? {
		switch self {
		case .getSteps:
			return nil
		case .auth(let authUser):
			return authUser
		case .postSteps(let records):
			return records
		}
	}
}
