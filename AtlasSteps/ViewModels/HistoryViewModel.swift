// Created by Scott Symes

import Combine
import Foundation

@MainActor
class HistoryViewModel: ObservableObject {

	@Service private var apiService: APIProtocol

	@Published var data: [DateSteps] = []
	@Published var error: Error? {
		didSet {
			if let error { print(error) } // TODO: Handle errors in UI
		}
	}
	@Published var daysIndex = 0
	@Published private var stepsData: [StepsRecord] = []

	private var cancellables: [AnyCancellable] = []
	private var calendar: Calendar

	let daySelections = [7, 30]

	init(calendar: Calendar = .current) {
		self.calendar = calendar
		setupBindings()
		Task { [weak self] in
			await self?.fetchData()
		}
	}

}

private extension HistoryViewModel {

	func setupBindings() {
		Publishers.CombineLatest($stepsData, $daysIndex)
			.sink { [weak self] data, index in
				guard let self else { return }
				self.data = self.group(records: data, overDays: self.daySelections[index])
			}
			.store(in: &cancellables)
	}

	func fetchData() async {
		do {
			stepsData = try await apiService.call(.getSteps)
		} catch {
			self.error = error
		}
	}

	func group(records: [StepsRecord], overDays days: Int) -> [DateSteps] {
		let earliest = calendar.date(byAdding: .day, value: -days, to: calendar.startOfDay(for: .now))!
		let filteredRecords = filterRecords(records, earliest: earliest)
		if filteredRecords.isEmpty { return [] }

		var result: [DateSteps] = {
			(0..<days)
				.map {
					.init(
						date: calendar.date(
							byAdding: .day,
							value: -$0,
							to: calendar.startOfDay(for: .now)
						)!,
						steps: .init(0)
					)
				}
		}()

		for record in filteredRecords {
			let day = calendar.component(.day, from: record.date)
			if let index = result.firstIndex(where: { calendar.component(.day, from: $0.date) == day }) {
				result[index].steps = .init(result[index].steps.stepCount + record.stepsInPeriod)
			}
		}
		return result
	}

	func filterRecords(_ records: [StepsRecord], earliest: Date) -> [StepsRecord] {
		records
			.filter { $0.date <= .now }
			.filter { $0.date >= earliest }
	}

}
