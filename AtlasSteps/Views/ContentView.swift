// Created by Scott Symes

import SwiftUI

struct ContentView: View {

    var body: some View {
			TabView {
				Today(viewModel: .init())
					.tabItem {
						Label("Today", systemImage: "figure.walk")
					}
				History(viewModel: .init())
					.tabItem {
						Label("History", systemImage: "calendar.badge.clock")
					}
			}
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
			ContentView()
    }
}
