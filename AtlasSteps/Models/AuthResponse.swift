// Created by Scott Symes

import Foundation

struct AuthResponse: Codable {
	let jwt: String
	let user: User

	struct User: Codable {
		let id: Int
		let username: String
		let email: String
		let provider: String
		let confirmed: Bool
		let blocked: Bool
		let created_at: Date
		let updated_at: Date
		let role: Role

		struct Role: Codable {
			let id: Int
			let name: String
			let description: String
			let type: String
		}
	}
}


