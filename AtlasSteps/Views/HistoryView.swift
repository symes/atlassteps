// Created by Scott Symes

import SwiftUI

struct History: View {

		@ObservedObject var viewModel: HistoryViewModel

    var body: some View {
			VStack {

				Picker("Days", selection: $viewModel.daysIndex) {
					ForEach(viewModel.daySelections.indices, id: \.self) { index in
						Text(String(self.viewModel.daySelections[index]) + " days")
					}
				}.pickerStyle(SegmentedPickerStyle())

				Spacer()

				if $viewModel.data.isEmpty {
					Text("No data in date range")
				} else {
					HistoryStepChart(data: $viewModel.data)
				}

				Spacer()

			}
			.padding(.all, 20)
    }

}

struct History_Previews: PreviewProvider {
    static var previews: some View {
			History(viewModel: .init())
    }
}
