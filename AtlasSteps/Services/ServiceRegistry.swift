// Created by Scott Symes

import Foundation

// Quick and dirty DI class using a singleton & a dictionary with string keys based on type name
// There are many open-source packages that could be used as a more production-ready & robust solution

protocol ServiceRegistryProtocol {
	func register<Service>(type: Service.Type, service: Any)
	func resolve<Service>(type: Service.Type) -> Service?
}

final class ServiceRegistry: ServiceRegistryProtocol {

	static let shared = ServiceRegistry()

	private init() {}

	private var services: [String: Any] = [:]

	func register<Service>(type: Service.Type, service: Any) {
		services["\(type)"] = service
	}

	func resolve<Service>(type: Service.Type) -> Service? {
		return services["\(type)"] as? Service
	}

}

extension ServiceRegistry {
#if targetEnvironment(simulator)
	func registerAll() {
		Self.shared.register(type: APIProtocol.self, service: APIService())
		Self.shared.register(type: LocalStorageProtocol.self, service: LocalStorageService())
		Self.shared.register(type: PedometerProtocol.self, service: MockPedometerService())
	}
#else
	func registerAll() {
		Self.shared.register(type: APIProtocol.self, service: APIService())
		Self.shared.register(type: LocalStorageProtocol.self, service: LocalStorageService())
		Self.shared.register(type: PedometerProtocol.self, service: PedometerService())
	}
#endif
}

@propertyWrapper struct Service<Value> {

	private let container: ServiceRegistryProtocol
	private let `default`: Value?

	init(container: ServiceRegistryProtocol? = nil, `default`: Value? = nil) {
		self.container = container ?? ServiceRegistry.shared
		self.default = `default`
	}

	lazy var wrappedValue: Value = {
		if let value = container.resolve(type: Value.self) {
			return value
		} else if let `default` {
			return `default`
		} else {
			fatalError("Could not resolve type \(Value.self) and default is nil")
		}
	}()

}
