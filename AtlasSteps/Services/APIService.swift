// Created by Scott Symes

import CryptoKit
import Foundation

protocol APIProtocol {
	func call<T: Codable>(_ endpoint: APIEndpoint) async throws -> T
}

/// Service to interact with API decribed at: https://testapi.mindware.us/documentation/v1.0.0#/Steps
/// N.B. post endpoint doesn't update records received in subsequent `get` operations. Data seems to be ignored
final class APIService: APIProtocol {

	enum APIError: Error {
		case invalidResponse(URLResponse)
		case decodingError(Data, DecodingError)
		case notAuthorized
		case invalidDate
		case mismatchedUser
		case invalidURLComponents
	}

	private let baseURL = "https://testapi.mindware.us/"

	private var authToken: String?
	private var userID: String?
	private var username: String?

	private let urlSession: URLSession
	private let jsonDecoder: JSONDecoder = .init()
	private let jsonEncoder: JSONEncoder = .init()

	init(urlSession: URLSession = .shared) {
		self.urlSession = urlSession

		let formatter = DateFormatter()
		formatter.calendar = Calendar(identifier: .iso8601)
		formatter.locale = Locale(identifier: "en_US_POSIX")
		formatter.timeZone = TimeZone(secondsFromGMT: 0)

		let isoFormatter = ISO8601DateFormatter()
		isoFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

		jsonDecoder.dateDecodingStrategy = .custom({ decoder -> Date in
			let container = try decoder.singleValueContainer()
			let dateStr = try container.decode(String.self)
			if let date = isoFormatter.date(from: dateStr) {
				return date
			}
			formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
			if let date = formatter.date(from: dateStr) {
				return date
			}
			formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXXXX"
			if let date = formatter.date(from: dateStr) {
				return date
			}
			throw APIError.invalidDate
		})
		self.jsonEncoder.dateEncodingStrategy = .custom({ date, encoder in
			let stringData = isoFormatter.string(from: date)
			var container = encoder.singleValueContainer()
			try container.encode(stringData)
		})
	}

	/// `user` default: only valid credentials for this API
	private func authorize(_ user: UserAuth = .user1) async throws {
		if let userID, user.id == userID, authToken != nil {
			return // we already have a valid token for this user
		}
		let request = try request(for: .auth(user))
		let (data, response) = try await urlSession.data(for: request)
		// perform basic validation
		guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
			throw APIError.invalidResponse(response)
		}
		let authResponse = try jsonDecoder.decode(AuthResponse.self, from: data)
		guard authResponse.user.email == user.id else {
			throw APIError.mismatchedUser
		}
		self.authToken = authResponse.jwt // there is no refresh token
		self.userID = authResponse.user.email
		self.username = "scott" // authResponse.user.username // TODO: get from user service instead.
		// we're using hard-coded user here instead as auth currently only works with `user1`
	}

	func call<T: Codable>(_ endpoint: APIEndpoint) async throws -> T {
		try await authorize()
		let request = try request(for: endpoint)
		let (data, response) = try await urlSession.data(for: request)

		// perform basic validation
		guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
			throw APIError.invalidResponse(response)
		}
		return try jsonDecoder.decode(T.self, from: data)
	}

	private func request(for endpoint: APIEndpoint) throws -> URLRequest {
		var url = URL(string: baseURL)! // safe to force-unwrap hardcoded url or it wouldn't run in debug
		url.append(path: endpoint.path)

		var request = URLRequest(url: url)
		request.httpMethod = endpoint.method.rawValue
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.setValue("application/json", forHTTPHeaderField: "Accept")
		if case .auth = endpoint { /* blank */ } else {
			guard let authToken else {
				throw APIError.notAuthorized
			}
			request.setValue("Bearer " + authToken, forHTTPHeaderField: "Authorization")
		}
		if let modelData = endpoint.data {
			let data = try jsonEncoder.encode(modelData)
			request.httpBody = data
		}
		if case .getSteps = endpoint {
			let queryItems = [URLQueryItem(name: "_sort", value: "created_at:desc")]
			request.url?.append(queryItems: queryItems)
			// We can't simply add a URLQueryItem with name="=" as the = will be URL escaped. We need a work-around.
			// But the query parameter can be inserted directly into the `percentEncodedQuery` field of the `URLComponents`
			if let username,
				 let url = request.url,
				 var components = URLComponents(url: url, resolvingAgainstBaseURL: false) {
					// Insert the `=` query parameter directly at end of current query string
					components.percentEncodedQuery = (components.percentEncodedQuery ?? "") + "&==username:\(username)"
					if let urlString = components.string,
						 let newURL = URL(string: urlString) {
						request = URLRequest(url: newURL)
					} else {
						throw APIError.invalidURLComponents
					}
			}
		}
		return request
	}

}

enum HTTPMethod: String {
	case get = "GET"
	case put = "PUT"
	case post = "POST"
	case delete = "DELETE"
}
