// Created by Scott Symes

import Foundation

protocol LocalStorageProtocol {
	func add(record: StepsRecord) throws
	func getRecords() throws -> [StepsRecord]
	func replaceAll(with records: [StepsRecord]) throws
}

class LocalStorageService: LocalStorageProtocol {

	private let fileManager: FileManager
	private let calendar: Calendar
	private let jsonDecoder: JSONDecoder = .init()
	private let jsonEncoder: JSONEncoder = .init()
	private var startOfRecordsDay: Date?

	private static let filename = "steps.json"

	private var path: URL {
		fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(Self.filename)
	}

	init(
		fileManager: FileManager = .default,
		calendar: Calendar = .current
	) {
		self.fileManager = fileManager
		self.calendar = calendar
		self.jsonDecoder.dateDecodingStrategy = .iso8601
		self.jsonEncoder.dateEncodingStrategy = .iso8601
		guard let firstRecord = try? getRecords().first else { return }
		self.startOfRecordsDay = calendar.startOfDay(for: firstRecord.date)
	}

	func getRecords() throws -> [StepsRecord] {
		guard fileManager.fileExists(atPath: path.path()) else { return [] }
		let data = try Data(contentsOf: path)
		return try jsonDecoder.decode([StepsRecord].self, from: data)
	}

	func add(record: StepsRecord) throws {
		var records: [StepsRecord] = (try? getRecords()) ?? []
		records.append(record)
		try write(records: records)
	}

	func replaceAll(with records: [StepsRecord]) throws {
		try write(records: records)
	}

	private func write(records: [StepsRecord]) throws {
		let data = try jsonEncoder.encode(records)
		try data.write(to: path)
	}

}
