// Created by Scott Symes

import Foundation

struct UserAuth: Codable {
	let id: String
	let password: String

	enum CodingKeys: String, CodingKey {
		case id = "identifier"
		case password
	}
}

extension UserAuth {
	static var user1: Self {
		.init(id: "user1@test.com", password: "Test123!")
	}
}
