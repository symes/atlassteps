// Created by Scott Symes

import SwiftUI

struct Today: View {

	@ObservedObject var viewModel: TodayViewModel

	var body: some View {
		VStack {
			Spacer()
			StepsHeading(stepCount: $viewModel.stepCounter.stepCount)
			ProgressChart(steps: $viewModel.dailyStepCount)
			Spacer()
			DayStepChart(data: $viewModel.stepData)
			Spacer()
		}
		.padding(.all, 20)
	}
}

struct StepsHeading: View {

	@Binding var stepCount: Int

	var body: some View {
		VStack {
			Text("\(stepCount)")
				.font(.largeTitle)

			Text("Steps")
				.font(.title)
		}
		.fixedSize(horizontal: false, vertical: true)
	}
}

struct Today_Previews: PreviewProvider {
    static var previews: some View {
			Today(viewModel: .init())
    }
}
