// Created by Scott Symes

import Foundation

struct DateSteps: Identifiable {
	let date: Date
	var steps: PlottableStepCount
	var id: UUID {
		steps.id
	}
}

extension Array where Element == DateSteps, Self.Index == Int {

	mutating func append(_ record: StepsRecord) {
		let hour = Calendar.current.component(.hour, from: record.date)
		if hour >= 0, hour < self.count {
			self[hour].steps = PlottableStepCount(self[hour].steps.stepCount + record.stepsInPeriod)
		}
	}

}
