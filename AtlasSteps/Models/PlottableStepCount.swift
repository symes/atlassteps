// Created by Scott Symes

import Charts
import Foundation

struct PlottableStepCount: Identifiable, Plottable {
	typealias PrimitivePlottable = Int

	let id: UUID
	var stepCount: Int

	init(_ steps: Int) {
		self.id = .init()
		self.stepCount = steps
	}

	init?(primitivePlottable: Int) {
		self.id = .init()
		self.stepCount = primitivePlottable
	}

	var primitivePlottable: Int {
		stepCount
	}
}
