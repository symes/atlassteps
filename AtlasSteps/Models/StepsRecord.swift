// Created by Scott Symes

import Foundation

/*
 Fields from stagger:
 {
	 "id": "string",
	 "username": "string",
	 "steps_date": "string",
	 "steps_datetime": "Unknown Type: datetime",
	 "steps_count": 0,
	 "steps_total_by_day": 0
 }

Fields from live `https://testapi.mindware.us/steps` & discussion
{
	 "id":75, // record id
	 "username":"qtrang", // primary key
	 "steps_total":null, // ignore
	 "created_datetime":null, // ignore
	 "created_at":"2023-01-31T07:58:37.028Z", // ignore
	 "updated_at":"2023-01-31T07:58:37.028Z", // ignore
	 "steps_date":"2023-01-30", // ignore
	 "steps_datetime":"2023-01-31T07:58:36.979Z", // push to here
	 "steps_count":5060, // 5 min period - zeroed every 5 min / push
	 "steps_total_by_day":5060 // for 24hr period - zeroed every 24 hrs
}
 */

struct StepsRecord: Codable, Identifiable {
	var id: Int
	var username: String
	var date: Date
	var stepsInPeriod: Int
	var stepsInDay: Int

	enum CodingKeys: String, CodingKey {
		case id, username
		case date = "created_at"
		case stepsInPeriod = "steps_count"
		case stepsInDay = "steps_total_by_day"
	}

}
