// Created by Scott Symes

import SwiftUI

@main
struct AtlasStepsApp: App {

	init() {
		ServiceRegistry.shared.registerAll()
	}

	var body: some Scene {
		WindowGroup {
			ContentView()
		}
	}
}
