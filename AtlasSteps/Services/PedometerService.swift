// Created by Scott Symes

import Combine
import CoreMotion
import Foundation

protocol PedometerProtocol {
	var stepCountPublisher: AnyPublisher<Int, Never> { get }
	func startStepUpdates() throws
	func stepCountFrom(minutes: Int, before end: Date) async throws -> Int
	func stepCountSinceStartOfDay(until end: Date) async throws -> Int
}

enum PedometerServiceError: Error {
	case stepCountingUnavailable
	case noPedometerData
}

class PedometerService: PedometerProtocol {

	private let pedometer = CMPedometer()

	private var step = PassthroughSubject<Int, Never>()
	var stepCountPublisher: AnyPublisher<Int, Never> {
		step.eraseToAnyPublisher()
	}

	init() {
		do {
			try startStepUpdates()
		} catch {
			print(error) // TODO: handle errors
		}
	}

	deinit {
		pedometer.stopUpdates()
	}

	func startStepUpdates() throws {
		guard CMPedometer.isStepCountingAvailable() else {
			throw PedometerServiceError.stepCountingUnavailable
		}
		pedometer.startUpdates(from: .now) { [weak self] pedometerData, error in
			guard let pedometerData, error == nil else { return }
			self?.step.send(pedometerData.numberOfSteps.intValue)
		}
	}

	func stepCountFrom(minutes: Int = 5, before end: Date = .now) async throws -> Int {
		guard CMPedometer.isStepCountingAvailable() else {
			throw PedometerServiceError.stepCountingUnavailable
		}
		let start = end.addingTimeInterval(Double(-60 * minutes))
		return try await withCheckedThrowingContinuation { continuation in
			pedometer.queryPedometerData(from: start, to: end) { pedometerData, error in
				guard let pedometerData else {
					continuation.resume(throwing: PedometerServiceError.noPedometerData)
					return
				}
				if let error {
					continuation.resume(throwing: error)
					return
				}
				continuation.resume(returning: pedometerData.numberOfSteps.intValue)
			}
		}
	}

	func stepCountSinceStartOfDay(until end: Date = .now) async throws -> Int {
		guard CMPedometer.isStepCountingAvailable() else {
			throw PedometerServiceError.stepCountingUnavailable
		}
		let start = Calendar.current.startOfDay(for: end)
		return try await withCheckedThrowingContinuation { continuation in
			pedometer.queryPedometerData(from: start, to: end) { pedometerData, error in
				guard let pedometerData else {
					continuation.resume(throwing: PedometerServiceError.noPedometerData)
					return
				}
				if let error {
					continuation.resume(throwing: error)
					return
				}
				continuation.resume(returning: pedometerData.numberOfSteps.intValue)
			}
		}
	}

}
