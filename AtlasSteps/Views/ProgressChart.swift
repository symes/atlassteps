// Created by Scott Symes

import Charts
import SwiftUI

struct ProgressChart: View {

	@Binding var steps: PlottableStepCount

	var body: some View {
		VStack {
			Chart([steps]) { stepCount in
				BarMark(
					x: .value("Steps", stepCount),
					width: .fixed(8)
				)
				.mask { RectangleMark() }
			}
			.chartXScale(domain: [0, 10_000])
			.chartXAxis(.hidden)
			.fixedSize(horizontal: false, vertical: true)
			.border(.gray)

			Text("10,000")
				.frame(maxWidth: .infinity, alignment: .trailing)
				.padding(.trailing, 8)
		}
	}

}

struct ProgressChart_Previews: PreviewProvider {
    static var previews: some View {
			ProgressChart(steps:
					.init(
						get: {
							.init(4567)
						},
						set: { _ in }
					)
			)
    }
}
