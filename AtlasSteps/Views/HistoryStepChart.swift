// Created by Scott Symes

import Charts
import SwiftUI

struct HistoryStepChart: View {

	@Binding var data: [DateSteps]

	var body: some View {
		Chart(data) { data in
			BarMark(
				x: .value("Time", data.date, unit: .day),
				y: .value("Step Count", data.steps)
			)
		}
		.chartXAxis {
			AxisMarks(values: .stride(by: .day)) { value in
				if data.count < 14 {
					AxisValueLabel(
						format: .dateTime.day(),
						centered: true
					)
					AxisTick()
				} else if let date = value.as(Date.self) {
					let day = Calendar.current.component(.day, from: date)
					if day.isMultiple(of: 7) {
						AxisValueLabel(format: .dateTime.day())
						AxisTick()
					}
				}
			}
		}
		.frame(height: 300)
	}

}

struct HistoryStepChart_Previews: PreviewProvider {
	static var previews: some View {
		HistoryStepChart(data:
				.init(
					get: {
						(0..<30)
							.map {
								.init(
									date: Calendar.current.date(byAdding: .day, value: -$0, to: .now)!,
									steps: .init(Int.random(in: 500..<12_000))
								)
							}
					},
					set: { _ in }
				)
		).previewDisplayName("30 Days")

		HistoryStepChart(data:
				.init(
					get: {
						(0..<7)
							.map {
								.init(
									date: Calendar.current.date(byAdding: .day, value: -$0, to: .now)!,
									steps: .init(Int.random(in: 500..<12_000))
								)
							}
					},
					set: { _ in }
				)
		).previewDisplayName("7 Days")
	}
}
