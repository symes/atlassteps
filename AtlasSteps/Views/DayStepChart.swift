// Created by Scott Symes

import Charts
import SwiftUI

struct DayStepChart: View {

	@Binding var data: [DateSteps]

	var body: some View {
		Chart(data) { stepData in
			BarMark(
				x: .value("Time", stepData.date, unit: .hour),
				y: .value("Step Count", stepData.steps)
			)
		}
		.chartXAxis {
			AxisMarks(values: .stride(by: .hour, count: 3)) { value in
				if let date = value.as(Date.self) {
					let hour = Calendar.current.component(.hour, from: date)
					switch hour {
					case 0, 12:
						AxisValueLabel(format: .dateTime.hour())
					default:
						AxisValueLabel(format: .dateTime.hour(.defaultDigits(amPM: .omitted)))
					}
					AxisTick()
				}
			}
		}
		.frame(height: 260)
	}

}

struct DayStepChart_Previews: PreviewProvider {
    static var previews: some View {
			DayStepChart(
				data: .init(
					get: {
						let startOfDay = Calendar.current.startOfDay(for: .now)
						return (0..<24)
							.map {
								.init(
									date: Calendar.current.date(byAdding: .hour, value: $0, to: startOfDay)!,
									steps: .init(Int.random(in: 0..<300))
								)
							}
					},
					set: { _ in })
			)
    }
}
